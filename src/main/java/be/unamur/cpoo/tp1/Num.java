package be.unamur.cpoo.tp1;

/**
 * Created by GillesBodart on 6/05/2016.
 */
public class Num {
    // OVERVIEW : fournit des routines sur les nombres

    /**
     * @Requires a > 0, b > 0
     * @param a first number
     * @param b second number
     * @return renvoie le pgcd de n et d calculé par récursivité
     */
    public static int pgcd(int a, int b) {

        if (a > b && a*b!=0) {
            return pgcd(b, a % b);
        }else if (b > a && a*b!=0){
            return pgcd(a, b % a);
        }else {
            return a;
        }
    }

    /**
     * @Requires val > 0
     * @param val the value
     * @return a boolean, true if the val is prime
     */
    public static boolean isPrime(int val){
        boolean prime = true;
        int cpt = 1;
        while (prime && cpt < val/2){
            prime = pgcd(cpt,val) == 1;
            cpt++;
        }
        return prime;
    }

    public static int sum(int[] a){
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;
    }

    public static int [] combine(int[]a,int []b){
        int sumB = sum(b);
        int [] tmp = new int[a.length];
        for (int i=0;i<a.length;i++){
            tmp[i]=a[i]*sumB;
        }
        return tmp;
    }


}
