package be.unamur.cpoo.tp1;

/**
 * Created by GillesBodart on 6/05/2016.
 */
public class Transpose {

    /**
     * @param tab the matix
     * @return the transpose of the matrix
     * @Requires tab is a matrix (3,2)
     */
    public static int[][] exec(int[][] tab) {
        int[][] tmp = new int[2][3];
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                tmp[j][i] = tab[i][j];
            }
        }
        return tmp;
    }

}
