package be.unamur.cpoo.tp1;

/**
 * Created by GillesBodart on 6/05/2016.
 */
public class NumTest {

    public static void main (String [] args){
        ex31(33,110);
        ex32(15);
        int[]a = {1,20,3,4,5};
        int[]b = {9,5,3,7,1};
        ex34(a);
        ex35(a,b);
    }

    private static void ex31(int a, int b){
        int pgcd = Num.pgcd(a,b);
        System.out.println(String.format("The greatest common divisor between %d and %d is : %d",a,b,pgcd));
    }

    private static void ex32(int val){
        boolean prime = Num.isPrime(val);
        System.out.println(String.format("%d is%s prime",val,prime?"":"n't"));
    }
    private static void ex34(int [] a){
        int sum = Num.sum(a);
        System.out.println(String.format("the sum is %d ",sum));
    }
    private static void ex35(int [] a,int [] b){
        int [] combine = Num.combine(a,b);
        System.out.print("the elements are ( ");
        for(int cc : combine){
            System.out.print(cc+" ");
        }
        System.out.print (" )");
    }

}
