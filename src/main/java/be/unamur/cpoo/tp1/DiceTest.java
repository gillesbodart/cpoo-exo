package be.unamur.cpoo.tp1;

/**
 * Created by GillesBodart on 6/05/2016.
 */
public class DiceTest {

    public static void main(String[] args) {
        try {
            ex232();
            ex233();
        } catch (IllegalAccessException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Exercise 2.3.2
     *
     * @throws IllegalAccessException If the value is asked before rolling the dice
     */
    private static void ex232() throws IllegalAccessException {
        Dice d = new Dice();
        d.roll();
        int val = d.getVal();
        System.out.println(val);

    }

    /**
     * Exercise 2.3.3
     *
     * @throws IllegalAccessException If the value is asked before rolling the dice
     */
    private static void ex233() throws IllegalAccessException {
        Dice d1 = new Dice();
        Dice d2 = new Dice();
        //Dices are thrown once to have the first value
        d1.roll();
        d2.roll();
        int tries = 1;
        while (d1.getVal() != 6 && d1.getVal() != d2.getVal()) {
            d1.roll();
            d2.roll();
            tries++;
        }
        System.out.println(String.format("%d %s",tries,tries==1?"try":"tries"));
    }
}
