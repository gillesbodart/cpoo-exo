package be.unamur.cpoo.tp1;

/**
 * Created by GillesBodart on 6/05/2016.
 */
public class TransposeTest {

    public static void main(String [] args){
        int[][] tab = new int [3][2];
        tab[0][0] = 1;tab[0][1] = 2;tab[1][0] = 3;tab[1][1] = 4;tab[2][0] = 5;tab[2][1] = 61;
        System.out.println(Transpose.exec(tab).length);
    }
}
