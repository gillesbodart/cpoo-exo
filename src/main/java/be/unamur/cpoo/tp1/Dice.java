package be.unamur.cpoo.tp1;

import java.util.Random;

/**
 * Created by GillesBodart on 6/05/2016.
 */
public class Dice {

    private Integer val;
    private static Random r = new Random();
    private static final int MAX_FACES = 6;

    public Dice() {

    }

    public int roll(){
        this.val = r.nextInt(MAX_FACES)+1;
        return this.val;
    }

    public int getVal() throws IllegalAccessException {
        if (null == val) throw new IllegalAccessException("You have to throw your dices once befor getting a value");
        return val;
    }
}
