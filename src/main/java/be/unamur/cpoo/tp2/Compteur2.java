package be.unamur.cpoo.tp2;

/**
 * @OVERVIEW this Class can be use to increment or decrement a specified sequence
 * Created by GillesBodart on 6/05/2016.
 */
public class Compteur2 {

    private int sequence = -5;

    /**
     * @REQUIRES : /
     * @MODIFIERS : this.sequence
     * @EFFECT : this.sequence will be incremented by 1
     */
    public int incr() {
        this.sequence = sequence + 1;
        return sequence;
    }

    /**
     * @REQUIRES : /
     * @MODIFIERS : this.sequence
     * @EFFECT :  it will decrement sequence by step of 1;
     */
    public int decr() throws NotUnderZeroException {
        this.sequence = sequence - 1;
        return sequence;
    }
}
