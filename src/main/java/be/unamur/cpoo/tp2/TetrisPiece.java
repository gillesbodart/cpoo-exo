package be.unamur.cpoo.tp2;

/**
 * Created by GillesBodart on 6/05/2016.
 */
public class TetrisPiece {

    /**
     * @REQUIRES : /
     * @MODIFIES : /
     * @EFFECT : return the max X coordinate of this Tetris piece
     */
    private int getLargeur(){
        return 0;
    }

    /**
     * @REQUIRES : /
     * @MODIFIES : /
     * @EFFECT : return the max Y coordinate of this Tetris piece
     */
    private int getHauteur(){
        return 0;
    }

    /**
     * @REQUIRES : /
     * @MODIFIES : /
     * @EFFECT : return a tab filled with the lowest Y per X.
     */
    private int[] getJupe(){
        return null;
    }
}
