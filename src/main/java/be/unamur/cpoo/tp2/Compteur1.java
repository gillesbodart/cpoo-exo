package be.unamur.cpoo.tp2;

/**
 * @OVERVIEW this Class can be use to increment or decrement a specified sequence
 * Created by GillesBodart on 6/05/2016.
 */
public class Compteur1 {

    private int sequence = 2;

    /**
     * @REQUIRES : /
     * @MODIFIERS : this.sequence
     * @EFFECT : this.sequence will be doubled by 2
     */
    public int incr() {
        this.sequence = sequence *2 ;
        return sequence;
    }

    /**
     * @REQUIRES : /
     * @MODIFIERS : this.sequence
     * @EFFECT : if sequence = 0 then it will throw a NotUnderZeroException
     * else it will decrement sequence by step of 1;
     */
    public int decr() throws NotUnderZeroException {
        if (sequence == 0) throw new NotUnderZeroException("Sequence can't be negative");
        this.sequence = sequence - 1;
        return sequence;
    }

}
