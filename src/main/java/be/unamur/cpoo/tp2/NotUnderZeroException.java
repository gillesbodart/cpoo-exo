package be.unamur.cpoo.tp2;

/**
 * Created by GillesBodart on 6/05/2016.
 */
public class NotUnderZeroException extends Exception {
    public NotUnderZeroException() {
        super();
    }

    public NotUnderZeroException(String message ){
        super(message);
    }

    public NotUnderZeroException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotUnderZeroException(Throwable cause) {
        super(cause);
    }


}
