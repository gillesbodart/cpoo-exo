package be.unamur.cpoo.tp2;

import java.util.Random;

/**
 * @OVERVIEW This class represent a Dice in a real life
 * Created by GillesBodart on 6/05/2016.
 */
public class Dice {

    private Integer val;
    private static Random r = new Random();
    private static final int MAX_FACES = 6;

    /**
     * @EFFECT : create a new Dice without any value
     */
    public Dice() {

    }

    /**
     * @REQUIRE:
     * @MODIFIES: this.val
     * @EFFECT: the val will be randomly set between 1 and this.MAX_FACES
     */
    public int roll() {
        this.val = r.nextInt(MAX_FACES) + 1;
        return this.val;
    }

    /**
     * @REQUIRE: the Dice have to be roll at least once
     * @MODIFIES: /
     * @EFFECT: If the dice isn't been rolled, a IllegalAccessException will be launched
     * else it'll return the value of the dice
     */
    public int getVal() throws IllegalAccessException {
        if (null == val) throw new IllegalAccessException("You have to throw your dices once befor getting a value");
        return val;
    }
    
}
