package be.unamur.cpoo.tp2;

/**
 * @Overview Les MultiSets sont des ensembles non bornés d’entiers qui
 * peuvent contenir des doublons.
 * Ils sont mutables.
 * Un MultiSet typique est {x1,...,xn} où x1,...,xn sont des entiers.
 * Created by GillesBodart on 6/05/2016.
 */
public class MultiSet {

    /**
     * @REQUIRES : /
     * @MODIFIES : this
     * @EFFECT : this_post : this U {a}  which U can have multiples times the same value in the set
     */
    public void insert(int a) {
    }

    /**
     * @REQUIRES : /
     * @MODIFIES : this
     * @EFFECT : this_post : this \ {a} if {a} isn't in the set an NotFoundException will be thrown
     */
    public void remove(int a) throws NotFoundException{
    }

    /**
     * @REQUIRES : /
     * @MODIFIES : /
     * @EFFECT : return a boolean who represent the presence of {a} in the object
     */
    public boolean isIn(int a) {
        return true;
    }

    /**
     * @REQUIRES : /
     * @MODIFIES : /
     * @EFFECT : return the amount of elements in the MultiSet
     */
    public int size() {
        return 0;
    }

    /**
     * @REQUIRES : /
     * @MODIFIES : /
     * @EFFECT : return the amount of times that a is in the MultiSet
     */
    public int numberOfOccurence(int a){return 0;}

    public int choose(){return 0;}


}
