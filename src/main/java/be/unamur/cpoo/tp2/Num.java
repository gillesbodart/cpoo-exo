package be.unamur.cpoo.tp2;

/**
 * Created by GillesBodart on 6/05/2016.
 */
public class Num {


    /**
     * @Requires n > 0 and d > 0
     * @Effect Compute the greatest common divisor
     * @Modifies /
     */
    public static int pgcd(int n, int d) {

        if (d == 0)
            return n;
        else
            return pgcd(d, n % d);

    }


    /**
     * @Requires a not null and a.length > 0
     * @Effect if a is null throw a NullPointerException
     * if a is empty throw a ZeroLengthException
     * Compute the sum of all the elements of the tab
     * @Modifies /
     */
    public static int sum(int[] a)
            throws NullPointerException, ZeroLengthException {
        if (a == null) throw new NullPointerException("You should have instantiate a list");
        if (a.length == 0) throw new ZeroLengthException("Your list should have at least 1 element");
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;

    }

    /**
     * @Requires a and b not null and a>0 and b>0
     * @Effect Multiply each element of the a list by the sum of the b list
     * @Modifiers /
     */
    public static int[] combine(int[] a, int[] b) throws ZeroLengthException {
        if (null == a || null == b) throw new NullPointerException("You should have instantiate the lists");
        if (a.length == 0 || b.length == 0) throw new ZeroLengthException("Your list should have at least 1 element");
        int sumB = sum(b);
        int[] tmp = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            tmp[i] = a[i] * sumB;
        }
        return tmp;
    }

    /**
     * @REQUIRES : a est trié
     * @EFFECTS : si a == null, lance NullPointerException
     * sinon si x n’appartient pas à a, lance NotFoundException
     * sinon retourne i tel que a[i] == x
     */
    public static int search(int[] a, int x)
            throws NullPointerException, NotFoundException {
        if (null == a) throw new NullPointerException("a is null and shouldn't");
        int cpt = 0;
        boolean found = false;
        while (cpt < a.length) {
            if (a[cpt] == x) return cpt;
            cpt++;
        }
        throw new NotFoundException(String.format("%d isn't in the tab", x));

    }
}
