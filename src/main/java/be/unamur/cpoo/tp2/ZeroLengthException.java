package be.unamur.cpoo.tp2;

/**
 * Created by GillesBodart on 6/05/2016.
 */
public class ZeroLengthException extends Exception {

    public ZeroLengthException() {
    }

    public ZeroLengthException(String message) {
        super(message);
    }

    public ZeroLengthException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZeroLengthException(Throwable cause) {
        super(cause);
    }


}
