package dice;
import java.util.Random;

public class Dice {
	
	private int valeur;
	private Random rd;

	public Dice() { 
		rd = new Random();
		roll(); 
	}
	
	public void roll() {
		valeur = rd.nextInt(6) + 1;
	}
	
	public int getValue() {
		return valeur;
	}
	
}
