package dice;

public class DiceTest {

	public static void main(String[] args) {
		
		Dice monDe = new Dice();
		System.out.println("Création du dé : valeur = " + monDe.getValue());
		
		monDe.roll();
		System.out.println("Lancé du dé : valeur = " + monDe.getValue());
		System.out.println("Relecture du dé : valeur = " + monDe.getValue());
		
		monDe.roll();
		System.out.println("Lancé du dé : valeur = " + monDe.getValue());
		System.out.println("Relecture du dé : valeur = " + monDe.getValue());
		
		
	}
	
}
