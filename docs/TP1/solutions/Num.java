package arithmetique;

public class Num {
	// OVERVIEW : fournit des routines sur les nombres

	/**
	 * REQUIRES : n > 0 et d > 0 EFFECTS : renvoie le pgcd de n et d
	 */
	public static int pgcd(int n, int d) {

		if (d == 0)
			return n;
		else
			return pgcd(d, n % d);

	}

	public static int sum(int[] a) {
		int sum = 0;

		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}

		return sum;

	}

	public static int[] combine(int[] a, int[] b) {
		int[] c = new int[a.length];
		int somme = sum(b);
		for (int i = 0; i < a.length; i++) {
			c[i] = a[i] * somme;
		}
		return c;
	}

}
