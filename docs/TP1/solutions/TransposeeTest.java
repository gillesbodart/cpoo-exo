
public class TransposeeTest {

	public static void main(String[] args) {
		
		int[][] tab={ {2,0}, {-1,-4}, {3,5} };
		
		System.out.println("Tableau d'entree :\n " + afficheTab(tab));
		
		
		int[][] returnTab = Transposee.transposee(tab);
		
	
		System.out.println("Tableau transpose :\n " + afficheTab(returnTab));

	}
	

	public static String afficheTab(int[][] tab) {
		
		String affichage = new String();
		
		for (int i=0; i < tab.length; i++) {
			for (int j=0; j < tab[i].length; j++) {
				affichage = affichage + "[" + tab[i][j] + "] ";
			}
			affichage = affichage + "\n";
		}
		
		return affichage;
		
	}

}
