
public class Transposee {

	public static int[][] transposee(int[][] inputTab) {
		
		int[][] outputTab =  new int[2][3];
		
		for (int i=0; i < inputTab.length; i++) {
			for (int j=0; j < inputTab[i].length; j++) {
				outputTab[j][i] = inputTab[i][j];
			}
		}
		
		return outputTab; 
		
	}
	
}
