package arithmetique;

import java.util.Random;

public class NumTest {

	public static void main(String[] args) {

		System.out.println(Num.pgcd(24, 18));
		System.out.println(Num.pgcd(18, 24));
		System.out.println(Num.pgcd(3, 9));
		System.out.println(Num.pgcd(12, 8));

		Random rand = new Random();

		int[] a = new int[rand.nextInt(10)];
		for (int i = 0; i < a.length; i++) {
			a[i] = rand.nextInt(10);
		}
		System.out.print("a = [");
		for (int j = 0; j < a.length; j++) {
			System.out.print(a[j] + " ");
		}
		System.out.print("]\n");
		System.out.println("Somme A: " + Num.sum(a));
		int[] b = new int[rand.nextInt(10)];
		for (int i = 0; i < b.length; i++) {
			b[i] = rand.nextInt(10);
		}
		System.out.print("b = [");
		for (int j = 0; j < b.length; j++) {
			System.out.print(b[j] + " ");
		}
		System.out.print("]\n");
		System.out.println("Somme B: " + Num.sum(b));

		
		int [] c = Num.combine(a, b);
		System.out.print("c = [");
		for (int j = 0; j < c.length; j++) {
			System.out.print(c[j] + " ");
		}
		System.out.print("]");

	}

}
