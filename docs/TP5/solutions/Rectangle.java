
public class Rectangle {
	// OVERVIEW : A Rectangle is a geometrical rectangle 
	//				(c'est un quadrilatère qui a 4 angles droits)
	//				A Rectangle is defined 
	//					by his hauteur and his largeur
	//				A typical Rectangle is (hauteur, largeur)
	//				An example Rectangle is hauteur = 5, largeur = 4
	
	private float l;
	private float h;
	
	/**
	 * @REQUIRES : l>0 and h>0.
	 * @EFFECTS : Create a Rectangle 
	 * 				(who will have l as largeur and h as hauteur)
	 * 				this_post.hauteur = h
	 * 				this_post.largeur = l.
	 *	
	 */
	public Rectangle(float l, float h) {
		this.l = l;
		this.h = h;
	}
	
	/**
	 * @REQUIRES : true
	 * @MODIFIES : /
	 * @EFFECTS : return this.largeur.
	 * 
	 */
	public float donneLargeur() { return l; }
	
	/**
	 * @REQUIRES : true
	 * @MODIFIES : /
	 * @EFFECTS : return this.hauteur.
	 * 
	 */
	public float donneHauteur() { return h; }
	
	/**
	 * @REQUIRES : l > 0.
	 * @MODIFIES : this.
	 * @EFFECTS : this_post.largeur = l.
	 * 
	 */
	public void fixeLargeur(float l) { this.l = l; }
	
	/**
	 * @REQUIRES : h > 0.
	 * @MODIFIES : this.
	 * @EFFECTS : this_post.hauteur = h.
	 * 
	 */
	public void fixeHauteur(float h) { this.h = h; }
	
	/**
	 * @REQUIRES : true.
	 * @MODIFIES : /.
	 * @EFFECTS : return this.hauteur * this.largeur.
	 * 
	 */
	public float donneSurface() { return (l * h); }
}
