
public class Carre extends Rectangle {
	// OVERVIEW : A Carre is a geometrical carre 
		//				(c'est un quadrilatère qui a 4 angles droits et des côtés de même longueur)
		//				A Carre is defined 
		//					by his cote
		//				A typical Carre is (cote)
		//				An example Carre is cote = 5
		
	
	
	
	/**
	 * @REQUIRES : c>0
	 * @EFFECTS : crée un Carre
	 * 				this post.c= largeur
	 * 				this post.c= hauteur
	 * 
	 */
	public Carre(float c) { super(c, c); }
	
	
	/**
	 * @REQUIRES : c>0
	 * @MODIFIES : /
	 * @EFFECTS : this post.largeur= c
	 * 
	 */
	public void fixeLargeur(float c) {
		super.fixeLargeur(c);
		super.fixeHauteur(c);
	}
	
	/**
	 * @REQUIRES : c>0
	 * @MODIFIES : /
	 * @EFFECTS : this.hauteur = c
	 * 
	 */
	public void fixeHauteur(float c) {
		fixeLargeur(c);
	}
}
