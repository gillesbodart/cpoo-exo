package IntSet;

import java.util.*;

/**
 * @ OVERVIEW : Les IntSets sont des ensembles non bornés d'entiers.
 * Ils sont Mutables
 * Un IntSet Typique est {x1,...,xn} où x1,...,xn sont des entiers.
 *
 */
public class IntSet {

	// FA(c) = { c.otherEls[i].intValue() | 0<= i < c.otherEls.size() }
	//			U
	//		   { i | c.els[i] AND 0 <= i < c.els.length }
	
	// IRs:
	//	- c.els != null
	//  - c.otherEls != null
	//  - c.sz >= 0
	//	- pas de doublons dans c.otherEls
	//	- c.otherEls ne contient que des Integer
	//	- c.otherEls ne contient que des entiers <0 ou >=c.els.length
	//	- c.els n'est utilisé que pour des entiers i avec 0<=i<c.els.length
	//	- c.sz == count(c.els[i]) + c.otherEls.size() 
	
	private boolean[] els;
	private Vector otherEls;
	private int sz;

	/**
	 * @REQUIRES : true.
	 * @EFFECTS : Initialise this à l'ensemble vide
	 */
	public IntSet(){
		els = new boolean[100];
		otherEls = new Vector();
		sz = 0;
		if (!repOK()) throw new FailureException();
	}

	/**
	 * @REQUIRES : true.
	 * @MODIFIES : this.
	 * @EFFECTS : this_post = this U {x}.
	 */
	public void Insert(int x){
		Integer i = new Integer(x);

		if (!IsIn(x)) { 

			if(shouldGoInBooleanTab(x)){
				els[x]=true;
			}
			else
			{
				otherEls.add(i);
			}
			sz++;
		}
		if (!repOK()) throw new FailureException();
	}

	/**
	 * @REQUIRES : true.
	 * @MODIFIES : /.
	 * @EFFECTS : if 0 <= x < els.length
	 * 				then return true, 
	 * 				else return false;.
	 */

	private boolean shouldGoInBooleanTab(int x){
		if(0 <= x && x < els.length){
			return true;
		}
		
		return false;
		
	}


	/**
	 * @REQUIRES : true.
	 * @MODIFIES : this.
	 * @EFFECTS : this_post = this / {x}.
	 */
	public void Remove(int x){
		
		if(shouldGoInBooleanTab(x)){
			if(els[x]) {
				els[x]=false;
				sz--;
				if (!repOK()) throw new FailureException();
				return;
			}
		}
		
		int i = GetIndex(new Integer(x));
		if (i != -1 && otherEls.remove(i) != null) {
			sz--;
		}
		if (!repOK()) throw new FailureException();
		
	}

	/**
	 * @REQUIRES : true.
	 * @MODIFIES : this.
	 * @EFFECTS : if x is an element of this 
	 * 				then return true, 
	 * 				else false;.
	 */
	public boolean IsIn(int x){
		if(shouldGoInBooleanTab(x)){
			return els[x];
		}
		
		return GetIndex(new Integer(x)) >= 0;
	}

	/**
	 * @REQUIRES : x != null AND (x<0 OR x>99) 
	 * @MODIFIES : this
	 * @EFFECTS : (EXPOSE LA REP)
	 * 			   si x n'appartient pas à otherEls, retourne -1;  
	 * 			   sinon retourne l'index de x dans otherEls. 
	 */
	private int GetIndex(Integer x){
				
		for(int i=0; i < otherEls.size(); i++){
			if (x.equals(otherEls.get(i))){
				return i;
			}
		}
		return -1;
	}

	/**
	 * @REQUIRES : true.
	 * @MODIFIES : /.
	 * @EFFECTS : renvoie la cardinalité de this
	 */
	public int size(){
		return sz;
	}

	/**
	 * @REQUIRES : true.
	 * @MODIFIES : /.
	 * @EFFECTS : if this is empty
	 * 				then return EmptyException,
	 * 				else return x where x is in this.
	 */
	public int Choose() throws EmptyException{

		if (IsBoolTableEmpty() && otherEls.size() == 0){
			throw new EmptyException("IntSet.Choose");
		}

		if (otherEls.size() != 0){
			if (!repOK()) throw new FailureException();
			return ((Integer) otherEls.lastElement()).intValue();
		}
		
		for(int i=0; i<els.length; i++){
			if (els[i]){
				if (!repOK()) throw new FailureException();
				return i;
			}
		}
		if (!repOK()) throw new FailureException();
		return 0; // SHOULD NEVER REACH THIS LINE!
	}

	/**
	 * @REQUIRES : true.
	 * @MODIFIES : /.
	 * @EFFECTS : if all elements of boolTable is false
	 * 				then return true,
	 * 				else return false.
	 */
	private boolean IsBoolTableEmpty(){
		//		Check if boolean table is false for all elements 
		for (int i =0; i < els.length; i++){
			if (els[i]){
				if (!repOK()) throw new FailureException();
				return false;
			}
		}
		if (!repOK()) throw new FailureException();
		return true;
	}

	
	public Object clone(){
		IntSet intSetReturn = new IntSet();

		for(int i=0; i < els.length; i++){
			if (els[i]){
				intSetReturn.els[i] = true;
			}
		}

		for(int i=0; i < otherEls.size(); i++){
			intSetReturn.otherEls.add(otherEls.get(i));
		}
		
		if (!repOK()) throw new FailureException();
		return intSetReturn;
	}


	public String toString(){
		StringBuffer sb = new StringBuffer("IntSet: {");
		
		for(int i=0; i < els.length; i++){
			if (els[i]){
				sb.append(i + ", ");
			}
		}
		
		for(int i=0; i < otherEls.size(); i++){
			sb.append(((Integer)otherEls.get(i)).intValue() + ", ");
		}

		if (sb.length() > 9) {
			if(sb.charAt(sb.length()-2) == ',') {
				sb.delete(sb.length()-2, sb.length());
			}
		}
		
		sb.append("}");

		if (!repOK()) throw new FailureException();
		
		return sb.toString();
	}
	
	

	public boolean repOK(){

		if(sz < 0) return false;
		if(els == null) return false;
		if(otherEls == null) return false;
		//	- pas de doublons dans c.otherEls
		//	- c.otherEls ne contient que des Integer
		//	- c.otherEls ne contient que des entiers <0 ou >=c.els.length
		//	- c.els n'est utilisé que pour des entiers i avec 0<=i<c.els.length
		//	- c.sz == count(c.els[i]) + c.otherEls.size()

		return true;
	}
}
