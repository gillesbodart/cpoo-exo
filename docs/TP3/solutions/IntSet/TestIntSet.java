package IntSet;

public class TestIntSet {

	public static void main(String[] args) {

		// Intset vide	
		{
			System.out.println("IntSet vide");
			IntSet s = new IntSet();
			System.out.println(s);
		}

		// Intset avec 1 elem negatif	
		{
			System.out.println("IntSet 1 int negatif");
			IntSet s = new IntSet();
			s.Insert(-5);
			System.out.println(s);
		}

		// Intset avec 1 elem > 100	
		{
			System.out.println("IntSet 1 int > 100");
			IntSet s = new IntSet();
			s.Insert(142);
			System.out.println(s);
		}

		// Intset avec 1 elem 0<=i<=99	
		{
			System.out.println("IntSet 1 int 0<=i<=99");
			IntSet s = new IntSet();
			s.Insert(4);
			System.out.println(s);
		}

		// Intset avec 0	
		{
			System.out.println("IntSet avec 0");
			IntSet s = new IntSet();
			s.Insert(0);
			System.out.println(s);
		}

		// Intset avec 99	
		{
			System.out.println("IntSet avec 99");
			IntSet s = new IntSet();
			s.Insert(99);
			System.out.println(s);
		}

		// Intset avec 4 et demande taille	
		{
			System.out.println("IntSet avec 4 et demande taille");
			IntSet s = new IntSet();
			s.Insert(4);
			System.out.println("taille de s = " + s.size());
		}

		// Intset avec 142 et demande taille	
		{
			System.out.println("IntSet avec 142 et demande taille");
			IntSet s = new IntSet();
			s.Insert(142);
			System.out.println("taille de s = " + s.size());
		}
		
		// Intset avec -15 et demande taille	
		{
			System.out.println("IntSet avec -15 et demande taille");
			IntSet s = new IntSet();
			s.Insert(-15);
			System.out.println("taille de s = " + s.size());
		}

		// Intset vide et demande taille	
		{
			System.out.println("IntSet vide et demande taille");
			IntSet s = new IntSet();
			System.out.println("taille de s = " + s.size());
		}

		// Intset avec 142 et retire le 142	
		{
			System.out.println("IntSet avec 142 et retire le 142");
			IntSet s = new IntSet();
			s.Insert(142);
			System.out.println("taille de s = " + s.size());
			s.Remove(142);
			System.out.println("taille de s = " + s.size());
		}

		// Intset avec 142 et retire le 143	
		{
			System.out.println("IntSet avec 142 et retire le 142");
			IntSet s = new IntSet();
			s.Insert(142);
			System.out.println("taille de s = " + s.size());
			s.Remove(143);
			System.out.println("taille de s = " + s.size());
		}

		// Intset avec 142 et test si 142 dedans	
		{
			System.out.println("IntSet avec 142 et test si 142 dedans");
			IntSet s = new IntSet();
			s.Insert(142);
			System.out.println("taille de s = " + s.size());
			System.out.println("142 in s ? " + s.IsIn(142));
		}

		// Intset avec -5 et test si -5 dedans	
		{
			System.out.println("IntSet avec -5 et test si -5 dedans");
			IntSet s = new IntSet();
			s.Insert(-5);
			System.out.println("taille de s = " + s.size());
			System.out.println("-5 in s ? " + s.IsIn(-5));
		}

		// Intset avec 2 et test si 2 dedans	
		{
			System.out.println("IntSet avec 2 et test si 2 dedans");
			IntSet s = new IntSet();
			s.Insert(2);
			System.out.println("taille de s = " + s.size());
			System.out.println("2 in s ? " + s.IsIn(2));
		}

		// Intset avec 0 et test si 0 dedans	
		{
			System.out.println("IntSet avec 0 et test si 0 dedans");
			IntSet s = new IntSet();
			s.Insert(0);
			System.out.println("taille de s = " + s.size());
			System.out.println("0 in s ? " + s.IsIn(0));
		}

		// Intset avec 99 et test si 99 dedans	
		{
			System.out.println("IntSet avec 99 et test si 99 dedans");
			IntSet s = new IntSet();
			s.Insert(99);
			System.out.println("taille de s = " + s.size());
			System.out.println("99 in s ? " + s.IsIn(99));
		}

		// Intset avec 142 et 6 et -4 et test si 142 dedans	
		{
			System.out.println("Intset avec 142 et 6 et -4 et test si 142 dedans");
			IntSet s = new IntSet();
			s.Insert(142);
			s.Insert(-4);
			s.Insert(6);
			System.out.println(s);
			System.out.println("taille de s = " + s.size());
			System.out.println("142 in s ? " + s.IsIn(142));
		}





		

	}
}
