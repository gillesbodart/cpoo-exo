import java.util.Iterator;
import java.util.NoSuchElementException;

public class Num {


	/**
	 * @REQUIRES: true
	 * @MODIFIES: /
	 * @EFFECTS: retourne un générateur de tous les entiers 
	 * à partir de -100 et en ordre croissant
	 */
	public static Iterator allInteger(){	
		return new GenInteger();
		
	}
	

	private static class GenInteger implements Iterator{
		
		private int temp;
		
		GenInteger(){
			temp = -100;
		}
		
		public boolean hasNext(){
			return true;
		}
		
		public Object next(){
			return new Integer(temp++);
		}

		public void remove() {
			
		}
	}
	
	
	/**
	 * 
	 * voir enoncé
	 */
	public static Iterator filter(Iterator g, Check x) throws NullPointerException {
		if (g == null) {
			throw new NullPointerException("Num.filter(Iterator, Check)");
		}
		
		return new GenFilter(g, x);
		
	}
	
	private static class GenFilter implements Iterator{
		
		private Iterator extGen;
		private Check extCheck;
		private int temp;
		private boolean valueAvailable;
		
		GenFilter(Iterator g, Check x){
			extGen = g;
			extCheck = x;
			valueAvailable = false;
		}
		
		public boolean hasNext(){
			if (valueAvailable) return true;
			
			while (extGen.hasNext()){
				int i = ((Integer)extGen.next()).intValue();
				if (extCheck.checker(i)){
					temp = i;
					valueAvailable = true;
					return true;
				}
			}
			
			return false;
		}
		
		public Object next(){
			if (!hasNext()) throw new NoSuchElementException("Num.filter(Iterator, Check)");
			valueAvailable = false;
			return new Integer(temp);
		}

		public void remove() {
			// TODO Auto-generated method stub
			
		}
	}
	
	

	/**
	 * @REQUIRES:true
	 * @MODIFIES: /
	 * @EFFECTS: retourne true si i est un nombre premier;
	 * 			 sinon retourne false.
	 */
	public static boolean isPrime(int i) {
		
		if (i <= 1) return false;
		
		Iterator gen = allPrimes();
		
		while (gen.hasNext()) {
			
			int currentPrime = ((Integer)gen.next()).intValue();
			
			if (i == currentPrime) return true;
			if (i < currentPrime) return false;
			
		}
		
		return false; //should not be reached
		
	}
	
	/**
	 * @REQUIRES: true
	 * @MODIFIES:/
	 * @EFFECTS: retourne un générateur de nombres premiers
	 * qui génère tous les nombres premiers (sous forme d'Integer)
	 * une seule fois et en ordre croissant
	 */
	public static Iterator allPrimes() {
		return new GenPrimes();
	}
	
	private static class GenPrimes implements Iterator {

		private int p;
		
		GenPrimes() {
			p = 2;
		}
		
		public boolean hasNext() {
			if (p > 100) return false;
			return true;
		}

		public Object next() {
			boolean isNotPrime = true;
			int t = p + 1;
			
			if (p==2) {
				p = 3;
				return new Integer(2);
			} else { 
				if (p == 3) { 
					p++;
					return new Integer(3); 
				}
			}
			
			
			while (isNotPrime) {
				if ((t%2) != 0) {
					for (int i = 3; i<Math.sqrt(t)+1 ; i+= 2) {
						if ((t%i)==0) {
							isNotPrime = false;
						}
					}
				}
				else {
					isNotPrime = false;
				}
					
				if (isNotPrime) {
					p = t;
					return new Integer(p);
				}
				else {
					t++;
					isNotPrime = true;
				}
			}
			
			
			
			return new Integer(p);
		}

		public void remove() { }
		
	}
	
	
}
