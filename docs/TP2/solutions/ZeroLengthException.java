package arithmetique;

public class ZeroLengthException extends Exception {

	public ZeroLengthException() { super(); }
	public ZeroLengthException(String s) { super(s); }
	
}
