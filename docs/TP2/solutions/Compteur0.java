package compteur;

/**
 * 
 * @OVERVIEW: Un Compteur0 est un type de données mutable qui
 * est initialisé à 0. Il permet d’incrémenter et de décrémenter cette
 * valeur par pas de 1, sans toutefois autoriser la décrémentation 
 * en deça de 0 ;
 * Un Compteur0 est typique est i avec i appartient aux entiers naturels.
 */
public class Compteur0 {
		
	/**
	 * @REQUIRES: true
	 * @EFFECTS: this_post = 0.
	 * */
	public Compteur0 {}

	
	/**
	 * @REQUIRES: true
	 * @MODIFIES: this
	 * @EFFECTS: this_post = this + 1
	 */
	public void incr() {
		
	}
	
	
	/**
	 * @REQUIRES: true
	 * @MODIFIES: this
	 * @EFFECTS: si this > 0, retourne this_post = this - 1;
	 * 			 sinon this_post = 0.
	 */
	public void decr(){
		
	}
	
	
	
	
	
	
	
	
	
	

}
