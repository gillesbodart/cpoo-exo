package multiset;

/**
 * @OVERVIEW:
 */
public class Multiset {
	
	

	/**
	 * @REQUIRES:
	 * @EFFECTS: crée un multiset vide (i.e., taille 0) 
	 */
	public MultiSet() {}
	
	
	/**
	 * @REQUIRES:true
	 * @MODIFIES:this
	 * @EFFECTS:this_post = this U {i}
	 * 		avec U considéré comme union non-ensembliste
	 * 		(donc doublons autorisés)
	 */
	public void insert(int i) {	}
	
	/**
	 * @REQUIRES: true
	 * @MODIFIES: this
	 * @EFFECTS:si this.size == 0, lance EmptyMultiSetException;
	 * 			si i n'appartient pas à this, lance NotFoundException;
	 * 			sinon this_post = this \ {i} 
	 * 			avec \ opérateur qui enlève une des occurrences de i dans this. 
	 */
	public void removeOneOccurrence(int i) 
			throws EmptyMultiSetException, NotFoundException { }


	/**
	 * @REQUIRES: true
	 * @MODIFIES: this
	 * @EFFECTS:si this.size == 0, lance EmptyMultiSetException;
	 * 			si i n'appartient pas à this, lance NotFoundException;
	 * 			sinon this_post = this \ {i} 
	 * 			(donc enlève toutes les occurences de i dans this)
	 */
	public void removeAllOccurrences(int i)
			throws EmptyMultiSetException, NotFoundException { }
}


/**
 * @REQUIRES:
 * @MODIFIES:
 * @EFFECTS:
 */