package arithmetique;

/**
 * @OVERVIEW:fournit des routines sur les nombres
 */
public class Num {


	/**
	 * @REQUIRES:n > 0 AND d > 0
	 * @MODIFIES:/
	 * @EFFECTS: retourne le pgcd de n et d calculé 
	 *  selon la division euclidienne. (surspecif)
	 */
	public static int pgcd(int n, int d) {

		if (d == 0)
			return n;
		else
			return pgcd(d, n % d);

	}

	/**
	 * @REQUIRES : a != null AND a.length > 0
	 * @MODIFIES : /
	 * @EFFECTS : retourne la somme des éléments du tableau a
	 */

	/**
	 * @REQUIRES: true
	 * @MODIFIES: /
	 * @EFFECTS: si a == null, lance NullPointerException; 
	 * 			 si a.length == 0, lance une ZeroLengthException;
	 * 			 sinon retourne la somme des éléments du tableau a.
	 */
	public static int sum(int[] a) 
			throws NullPointerException, ZeroLengthException {

		if (a == null) throw new NullPointerException("Num.sum(int[])");
		if (a.length==0) throw new ZeroLengthException("Num.sum(int[])");

		int sum = 0;

		for (int i = 0; i < a.length; i++) {
			sum += a[i];
		}

		return sum;

	}

	/**
	 * @REQUIRES: a != null AND a.length > 0 ; b != null AND b.length > 0 ;
	 * @MODIFIES: /
	 * @EFFECTS: retourne un tableau d de longueur a.length et où chaque
	 * 			élément d[i] (0<=i<a.length) est égal à a[i]*Num.sum(b).
	 */
	public static int[] combine(int[] a, int[] b) {
		int[] c = new int[a.length];
		int somme = sum(b);
		for (int i = 0; i < a.length; i++) {
			c[i] = a[i] * somme;
		}
		return c;
	}



	public static int search (int[] a, int x) throws NullPointerException, NotFoundException {

		if (a == null) throw new NullPointerException("Num.search(int[],int)");
		if (a.length == 0) throw new NotFoundException("Num.search(int[],int)");

		for (int i=0; i < a.length; i++) {
			if (a[i] > x) throw new NotFoundException("Num.search(int[],int)");
			if (a[i] == x) return i;	
		}
		
		throw new NotFoundException("Num.search(int[],int)");
	}
}










